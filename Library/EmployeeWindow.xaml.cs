using System.Windows;

namespace Library;

public partial class EmployeeWindow : Window
{
    private Employee _employee;
    public EmployeeWindow(Employee employee)
    {
        InitializeComponent();
        _employee = employee;
    }
}