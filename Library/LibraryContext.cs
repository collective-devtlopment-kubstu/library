using Microsoft.EntityFrameworkCore;

namespace Library;

public class LibraryContext: DbContext
{
    public DbSet<User> Users { get; set; } = null!;
    public LibraryContext()
    {
        Database.EnsureDeleted();
        Database.EnsureCreated();
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=library.db");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // использование Fluent API
        base.OnModelCreating(modelBuilder);
    }
}