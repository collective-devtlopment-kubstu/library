﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Library
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void BT_Enter_OnClick(object sender, RoutedEventArgs e)
        {
            StringBuilder errors = new StringBuilder();
            if (string.IsNullOrEmpty(TB_Login.Text))
                errors.AppendLine("Укажите логин");
            if (string.IsNullOrEmpty(TB_Password.Text))
                errors.AppendLine("Укажите пароль");

            if (errors.Length > 0)
            {
                MessageBox.Show(errors.ToString());
                return;
            }

            try
            {
                switch (CB_Role.SelectedIndex)
                {
                    case 0:
                        try
                        {

                            Student? student = DekanatContext.GetContext().Students.FirstOrDefault(p =>
                                p != null && p.Telephone == TB_Login.Text && p.Password == TB_Password.Text);
                            if (student != null)
                            {
                                StudentWindow studentWindow = new StudentWindow(student);
                                studentWindow.Show();
                                Close();
                            }
                            else
                                MessageBox.Show("Неверно введены логин или пароль");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        break;
                    case 1:
                        try
                        {
                            Employee? teacher = DekanatContext.GetContext().Employees.FirstOrDefault(p =>
                                (p.Email == TB_Login.Text || p.Telephone == TB_Login.Text) && p.Password == TB_Password.Text);                            
                            if (teacher != null)
                            {
                                TeacherWindow teacherWindow = new TeacherWindow(teacher);
                                teacherWindow.Show();
                                Close();
                            }
                            else
                                MessageBox.Show("Неверно введены логин или пароль");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        break;
                    case 2:
                        try
                        {
                            Employee? employee = DekanatContext.GetContext().Employees.FirstOrDefault(p =>
                                (p.Email == TB_Login.Text || p.Telephone == TB_Login.Text) && p.Password == TB_Password.Text);                            
                            if (employee != null)
                            {
                                EmployeeWindow employeeWindow = new EmployeeWindow(employee);
                                employeeWindow.Show();
                                this.Close();
                            }
                            else
                                MessageBox.Show("Неверно введены логин или пароль");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        break;
                    default:
                        MessageBox.Show("Вы не выбрали роль для входа!");
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        // Placeholder for Login
        private void TB_Login_OnGotFocus(object sender, RoutedEventArgs e)
        {
            TB_Login.Text = TB_Login.Text == "Введите логин или телефон" ? "" : TB_Login.Text;
            TB_Login.Foreground = Brushes.Black;
        }

        private void TB_Login_OnLostFocus(object sender, RoutedEventArgs e)
        {
            TB_Login.Text = TB_Login.Text == "" ? "Введите логин или телефон" : TB_Login.Text;
            if (TB_Login.Text == "Введите логин или телефон")
                TB_Login.Foreground = Brushes.Gray;
        }
        
        // Placeholder for Password
        private void TB_Password_OnGotFocus(object sender, RoutedEventArgs e)
        {
            TB_Password.Text = TB_Password.Text == "Введите пароль" ? "" : TB_Password.Text;
            TB_Password.Foreground = Brushes.Black;
        }
        private void TB_Password_OnLostFocus(object sender, RoutedEventArgs e)
        {
            TB_Password.Text = TB_Password.Text == "" ? "Введите пароль" : TB_Password.Text;
            if (TB_Password.Text == "Введите пароль")
                TB_Password.Foreground = Brushes.Gray;
        }
    }
}